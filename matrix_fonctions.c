#include "matrix_fonctions.h"

error_code matrix_alloc(matrix *mat, __int32_t m, __int32_t n)
{
    mat->m = m;
    mat->n = n;
    mat->data = malloc(mat->m * sizeof(__int32_t*));
    for (size_t i = 0; i < mat->m; i++)
    {
        mat->data[i] = malloc(mat->n * sizeof(__int32_t));
    }
    return ok;
}

error_code matrix_init(matrix *mat, __int32_t m, __int32_t n, __int32_t val)
{
    matrix_alloc(mat, m, n);

    for (size_t i = 0; i < m; i++)
    {
        for (size_t a = 0; a < n; a++)
        {
            mat->data[i][a] = val;
        }
    }
    return ok;
}

void free_matrix(matrix *mat)
{
    for(int i = 0; i < mat->m; i++)
    {
        free(mat->data[i]);
    }

    free(mat->data);
}

error_code matrix_destroy(matrix*mat)
{
    free_matrix(mat);

    mat->data = NULL;
    mat->n = -1;
    mat->m = -1;
    return ok;
}

error_code matrix_init_from_array(matrix *mat, __int32_t m,
__int32_t n, __int32_t data[], __int32_t s)
{
    matrix_alloc(mat, m, n);

    if(s == m*n)
    {
        int index = 0;
        for(int i = 0; i < m; i++)
        {
            for(int a = 0; a < n; a++)
            {
                mat->data[i][a] = data[index];
                index++;
            }
        }
        return ok;
    }
    return err;
}

error_code matrix_clone(matrix *cloned, const matrix mat)
{
    matrix_alloc(cloned, mat.m, mat.n);

    for(int i = 0; i < mat.m; i++)
    {
        for(int a = 0; a < mat.n; a++)
        {
            cloned->data[i][a] = mat.data[i][a];
        }
    }

    return ok;
}

error_code matrix_transpose(matrix *transposed,
const matrix mat)
{
    matrix_alloc(transposed, mat.n, mat.m);

    for(int i = 0; i < mat.m; i++)
    {
        for(int a = 0; a < mat.n; a++)
        {
            transposed->data[a][i] = mat.data[a][a];
        }
    }
    return ok;
}

error_code matrix_print(const matrix mat)
{
    for(int i = 0; i<mat.m; i++)
    {
        for(int a = 0; a<mat.n; a++)
        {
            printf("%d ", mat.data[i][a]);
        }
        printf("\n");
    }
    printf("\n");
    return ok;
}

error_code matrix_extract_submatrix(matrix *sub, const matrix mat,
__int32_t m0, __int32_t m1, __int32_t n0, __int32_t n1)
{
    matrix_alloc(sub, m1 - m0, n1 - n0);
    __int32_t m = 0;
    __int32_t n = 0;

    for(int i = m0; i < m1; i++)
    {
        for(int a = n0; a < n1; a++)
        {
            sub->data[m][n] = mat.data[i][a];
            n++;
        }
        m++;
        n = 0;
    }
    return ok;
}

bool matrix_is_equal(matrix mat1, matrix mat2)
{
    if(mat1.m != mat2.m || mat1.n != mat2.n)
    {
        return false;
	}
    else
    {
        for(int i = 0; i<mat1.m; i++)
        {
            for(int a = 0; a<mat1.n;a++)
            {
                if(mat1.data[i][a] != mat2.data[i][a])
                {
                    return false;
                }
            }
        }
    }
	return true;
}

error_code matrix_get(__int32_t *elem, const matrix mat,
__int32_t ix, __int32_t iy)
{
    if (ix >= mat.m || iy >= mat.n)
    {
        return err;
    }
	else if(mat.data[ix][iy] > INT_MAX || mat.data[ix][iy] < INT_MIN)
    {
        return err;
	}
    *elem = mat.data[ix][iy];
	return ok;
}

error_code matrix_set(matrix mat, __int32_t ix, __int32_t iy,
__int32_t elem)
{
    if (elem > INT_MAX || elem < INT_MIN)
    {
        return err;
    }
    else
    {
    	mat.data[ix][iy] = elem;
    }
	return ok;
}