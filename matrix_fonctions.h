#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <limits.h>

#ifndef _MATRIX_
#define _MATRIX_

typedef enum _error_code {
ok, err
} error_code;

typedef struct _matrix
{
    __int32_t m, n;
    __int32_t ** data;
}matrix;

error_code matrix_alloc(matrix *mat, __int32_t m, __int32_t n);

error_code matrix_init(matrix *mat, __int32_t m, __int32_t n, __int32_t val);

void free_matrix(matrix *mat);

error_code matrix_destroy(matrix *mat);

error_code matrix_init_from_array(matrix *mat, __int32_t m, __int32_t n, __int32_t data[], __int32_t s);

error_code matrix_clone(matrix *cloned, const matrix mat);

error_code matrix_transpose(matrix *transposed, const matrix mat);

error_code matrix_print(const matrix mat);

error_code matrix_extract_submatrix(matrix *sub, const matrix mat, __int32_t m0, __int32_t m1, __int32_t n0, __int32_t n1);

bool matrix_is_equal(matrix mat1, matrix mat2);

error_code matrix_get(__int32_t *elem, const matrix mat, __int32_t ix, __int32_t iy);

error_code matrix_set(matrix mat, __int32_t ix, __int32_t iy, __int32_t elem);

#endif