compute: matrix_fonctions.o matrix_compute.o
	gcc $^ -lm -o $@ -fsanitize=address

matrix_fonctions.o: matrix_fonctions.c matrix_fonctions.h
	gcc $< -c -fsanitize=address

matrix_compute.o: matrix_compute.c
	gcc $< -c -fsanitize=address

clean:
	rm -f *.o $@

rebuild: clean $@