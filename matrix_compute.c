#include "matrix_fonctions.h"
#include <time.h>

int main()
{
    srand(time(NULL));

    __int32_t m = 7;
    __int32_t n = 4;

    __int32_t tab[n];
    __int32_t *data[m];

    for (int i = 0; i < m; i++)
    {
        data[i] = tab;
    }

    //Allocation d'une matrice de test
    matrix matriceTest = {m, n, data};
    matrix_alloc(&matriceTest, matriceTest.m, matriceTest.n);

    //Initialisation d'une matrice de test avec comme valeur par defaut 15
    free_matrix(&matriceTest);
    matrix_init(&matriceTest, matriceTest.m, matriceTest.n, 15);
    printf("Test matrix : \n");
    matrix_print(matriceTest);

    //Clonage de la matrice de test
    matrix matriceClone = {m, n, data};
    matrix_clone(&matriceClone, matriceTest);
    printf("Cloned matrix : \n");
    matrix_print(matriceClone);

    //Transposee de la matrice de test
    matrix matriceTranspose = {m, n, data};
    printf("Transposed matrix : \n");
    matrix_transpose(&matriceTranspose, matriceTest);
    matrix_print(matriceTranspose);


    //Matrice cree depuis le tableau
    __int32_t s = m*n;
    __int32_t donnees[s];
    for(int i = 0; i < s; i++)
    {
        donnees[i] = rand() % 50;
    }
    free_matrix(&matriceTest);
    matrix_init_from_array(&matriceTest, matriceTest.m, matriceTest.n, donnees, s);
    printf("New matrix created from a random array : \n");
    matrix_print(matriceTest);


    //Submatrix de la matrice de test
    int m1 = 4, m0 = 2, n1 = 4, n0 = 2;
    __int32_t tab_sub[n1-n0];
    __int32_t *data_sub[m1-m0];

    for (int i = 0; i < (m1-m0); i++)
    {
        data_sub[i] = tab_sub;
    }
    matrix submatrice = {(m1 - m0), (n1 - n0), data_sub};
    matrix_extract_submatrix(&submatrice, matriceTest, m0, m1, n0, n1);
    printf("Submatrix extracted : \n");
    matrix_print(submatrice);

    //Choix d'une valeur aleatoire de la matrice
    int elem;
    int x = rand() % m;
    int y = rand() % n;
    matrix_get(&elem, matriceTest, x, y);
    printf("Chosen random value: %d\n\n", elem);

    //Changement de l'element choisi de la matrice juste avant
    matrix_set(matriceTest, x, y, rand() % 50);
    printf("Matrix with the random value chosed before changed : \n");
    matrix_print(matriceTest);

    //Liberation de la memoire
    matrix_destroy(&matriceTest);
    matrix_destroy(&matriceClone);
    matrix_destroy(&matriceTranspose);
    matrix_destroy(&submatrice);
}