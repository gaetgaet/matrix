#include "matrix_fonctions.h"
#include <assert.h>
#include <time.h>

int main()
{

    srand(time(NULL));

    __int32_t m = 5;
    __int32_t n = 5;

    __int32_t tab[n];
    __int32_t *data[m];

    for (int i = 0; i < m; i++)
    {
        data[i] = tab;
    }

    //Allocation d'une matrice de test
    matrix matriceTest = {m, n, data};
    assert(matrix_alloc(&matriceTest, matriceTest.m, matriceTest.n) == ok);


    //Initialisation d'une matrice de test avec comme valeur par defaut 15
    free_matrix(&matriceTest);
    assert(matrix_init(&matriceTest, matriceTest.m, matriceTest.n, 15) == ok);


    //Clonage de la matrice de test
    matrix matriceClone = {m, n, data};
    assert(matrix_clone(&matriceClone, matriceTest) == ok);


    //Transposee de la matrice de test
    matrix matriceTranspose = {m, n, data};
    assert(matrix_transpose(&matriceTranspose, matriceTest) == ok);


    //Matrice cree depuis le tableau
    __int32_t s = m*n;
    __int32_t donnees[s];
    for(int i = 0; i < s; i++)
    {
        donnees[i] = rand() % 50;
    }
    free_matrix(&matriceTest);
    assert(matrix_init_from_array(&matriceTest, matriceTest.m, matriceTest.n, donnees, s) == ok);


    //Submatrix de la matrice de test
    int m1 = 4, m0 = 2, n1 = 4, n0 = 2;
    __int32_t tab_sub[n1-n0];
    __int32_t *data_sub[m1-m0];

    for (int i = 0; i < (m1-m0); i++)
    {
        data_sub[i] = tab_sub;
    }
    matrix submatrice = {(m1 - m0), (n1 - n0), data_sub};
    assert(matrix_extract_submatrix(&submatrice, matriceTest, m0, m1, n0, n1) == ok);

    //Choix d'une valeur aleatoire de la matrice
    int element;
    int x = rand() % m;
    int y = rand() % n;
    assert(matrix_get(&element, matriceTest, x, y) == ok);

    //Changement de l'element choisi de la matrice juste avant
    assert(matrix_set(matriceTest, x, y, rand() % 50) == ok);

    //affichage de la matrice
    assert(matrix_print(matriceTest) == ok);

    //Liberation de la memoire
    assert(matrix_destroy(&matriceTest) == ok);
    matrix_destroy(&matriceClone);
    matrix_destroy(&matriceTranspose);
    matrix_destroy(&submatrice);
}